#!/usr/bin perl -w 

# @numbers=(100,'totot',12,4.365);
# print "@numbers\n";
# $numbers[-2] = 3;
# $"=" # ";


# %fruit_basket = ("pommes", "2kg", "kiwi", "500g");
# @fruits = keys %fruit_basket;
# @fruit = values %fruit_basket;
# print "@fruits\n";
# $fruits[-2] = 3;
# print "$fruits[1]\t";
# print "$fruit[1]\n";
# print "$fruits[0]\t";
# print "$fruit[0]\t";

use strict;

# my @numbers=(100,5,12,4, 'bobob');
# my @tab = map( {$_*$_} @numbers);
# my @tab1 = map( {4 * $_ * $_ -1} @numbers);
# my $SumResult = 0;
# map ({$SumResult+=$_} @numbers);
# my @maplong = map ({length($_)} @numbers);;

# print "table : @numbers\n";
# print "Table sum : $SumResult\n";
# print "Table square : @tab\n";
# print "function 4 x square - 1 : @tab1\n";
# print "Table size: @maplong\n";


my @tab_ARGV = map( {$_ * $_ * $_} @ARGV);
print "Cube : @tab_ARGV\n";
my $SumResult = 0;
my @tab = map ({$SumResult+=$_} @ARGV);
print "Table Sum : @tab\n";

my @result = map ({chop($_)} @ARGV);
print "Table Chop : @result\n";

my @result1 = map ({substr($_, 0, 1)} @ARGV);
print "Table substr : @result1\n";

my $min = $ARGV[$#ARGV];
print "result min : $min\n";