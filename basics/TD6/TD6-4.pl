use strict;


#saisie
# my @entree;
# my $ligne = <STDIN>;
# chomp($ligne);
# while ($ligne ne "FIN")
# {
# 	push(@entree,$ligne);
# 	$ligne = <STDIN>;
# 	chomp ($ligne);
# }

open(my $in, "<", "input.txt") or die "Impossible ouvrir input.txt: $!"
my @entree = <$in>;
close $in or die "$in : $!";

#tri longueur
sub tri_longueur { return length($a) <=> length($b); }

#tri par consonne
sub tri_consonne { 
	my $varA = $a =~ tr/aeiouyAEIOUY/aeiouyAEIOUY/;
	my $varB = $b =~ tr/aeiouyAEIOUY/aeiouyAEIOUY/;
	my $ConsonneA = length($a) - $varA;
	my $ConsonneB = length($b) - $varB;

	return $ConsonneA <=> $ConsonneB;
}

#occurence
sub occurences($$) 
{
	my ($a,$b) = @_;
	my @c = $b =~ /$a/g;
	my $count = @c;
	#printf("Le nbr occurences est : %d \n", $count);
	return $count;
}

#tri par occurence
sub tri_occurence {
	my $n1 = occurences($a,"toto");
	my $n2 = occurences($b,"toto");
	return $n1 <=> $n2;
}

my @in = ("Foo", "Bar toto", "Baz lol", "toto", "jerome");

my @out = sort tri_longueur @in;
my @out2 = sort tri_consonne @in;
my @out3 = sort tri_occurence @entree;
# printf ("@out\n");
# printf ("Consonne : @out2\n");
printf ("Occurences : @out3\n");