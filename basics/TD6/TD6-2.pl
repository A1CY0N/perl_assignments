#!/usr/bin perl -w 

use strict;

print "saisissez une premiere valeur : ";
my $entree1 = <stdin>;

print "saisissez une seconde valeur : ";
my $entree2 = <stdin>;

sub tri_longueur { return length($a) <=> length($b); }

sub tri_consonne { 
	my $varA = $a =~ tr/aeiouyAEIOUY/aeiouyAEIOUY/;
	my $varB = $b =~ tr/aeiouyAEIOUY/aeiouyAEIOUY/;
	my $ConsonneA = length($a) - $varA;
	my $ConsonneB = length($b) - $varB;

	return $ConsonneA <=> $ConsonneB;
}

sub occurences($$) 
{
	my ($a,$b) = @_;
	my @c = $b =~ /$a/g;
	my $count = @c;
	printf("Le nbr occurences est : %d \n", $count);
	return $count;
}

my @in = "je", "suis", "en", "DUT", "a", "Saint-Malo";

my @out = sort tri_longueur @in;
my @out2 = sort tri_consonne @in;
my @out3 = sort occurences(@in, "toto");
printf ("@out\n");
printf ("Consonne : @out2\n");
printf ("Occurence : @out3\n");



