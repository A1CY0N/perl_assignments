#!/usr/bin perl -w 

use strict;

 my @entree;
 my $ligne = <STDIN>;
 chomp($ligne);
 while ($ligne ne "FIN")
 {
 	push(@entree,$ligne);
 	$ligne = <STDIN>;
 	chomp ($ligne);
 }

#my @entree =("192.168.1.1", "192.168.1.2", "192.168.2.1", "10.10.10.1", "1.1.1.1");

sub tri_ip
{    
        my ($a1, $a2, $a3, $a4) = split /\./, $a;
        my ($b1, $b2, $b3, $b4) = split /\./, $b;
        
        if ($a1 == $b1) {
               
                if ($a2 == $b2)
                {
                      
                        if ($a3 == $b3)
                        {

                                $a4 =~ s/^([0-9]+)/$1/;
                                $b4 =~ s/^([0-9]+)/$1/;

                                return $a4 <=> $b4;
                        } else {
                              
                                
                                return $a3 <=> $b3;
                        }
                } else {
      
                        return $a2 <=> $b2;
                }
        } else {
               
                return $a1 <=> $b1;
        }
}

my @IP_trie = sort tri_ip @entree;
printf ("Adresses IP : @IP_trie\n");