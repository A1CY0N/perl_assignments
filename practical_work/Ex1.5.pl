#!/usr/bin/perl -w
use strict;
use warnings;

my %correspondances;
my @arp = `netstat -t -n`;
my $state;
my $out;

foreach my $corresp (@arp){
	if ($corresp =~ /tcp/){
		my @tab_netstat = split(/\s+/, $corresp);
		my @adresse_locale = split(/\:/, $tab_netstat[3]);
		my @adresse_distante = split(/\:/, $tab_netstat[4]);
		open ($out, ">", "$adresse_distante[0].txt") or die "Can't open $adresse_distante[0].txt: $!";
		$correspondances{"$adresse_locale[0]"}{$adresse_locale[1]}{$adresse_distante[0]}{$adresse_distante[1]} = $tab_netstat[5];
	}
}

while (1){
	foreach my $name (sort keys %correspondances) {
	    foreach my $subject (keys %{ $correspondances{$name} }) {
	        foreach my $adr_dist (keys %{ $correspondances{$name}{$subject} }) {
	        	foreach my $tablo (keys %{ $correspondances{$name}{$subject}{$adr_dist} }) {
	        		#print "$name, $subject, $adr_dist, $tablo  : $correspondances{$name}{$subject}{$adr_dist}{$tablo}\n";
	        		(my $s,my $m, my $h,my $j,my $M,my $a) = localtime();
	        		my @ping = `ping -w 5 $adr_dist`;
					foreach(@ping){
							my @temp = split (/\,/);
							my @pourc = split (/\s+/, $temp[2]);
							if($pourc[0] eq "0%"){
								$state = "allumé";
							}
							else{$state = "éteint";}
						}
						print "$j\/$M\/$a 	$h h $m 	Etat :  $state\n";
						print $out "$j\/$M\/$a 	$h h $m 	Etat :  $state\n";
	        	}
	        }
	    }
	}
}
sleep (60);