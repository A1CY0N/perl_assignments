#!/usr/bin/perl -w
use strict;
use warnings;

my %correspondances;
my @arp = `arp -a`;

foreach my $corresp (@arp){
	my @tab_arp = split(/\s+/, $corresp);
	$correspondances{"$tab_arp[1]"}{$tab_arp[3]} = $tab_arp[6];
}
foreach my $name (sort keys %correspondances) {
    foreach my $subject (keys %{ $correspondances{$name} }) {
        print "$name, $subject: $correspondances{$name}{$subject}\n";
    }
}