#!/usr/bin/perl -w
use strict;
use warnings;

my %correspondances;
my @arp = `netstat -t -n`;

foreach my $corresp (@arp){
	if ($corresp =~ /tcp/){
		my @tab_netstat = split(/\s+/, $corresp);
		my @adresse_locale = split(/\:/, $tab_netstat[3]);
		my @adresse_distante = split(/\:/, $tab_netstat[4]);
		$correspondances{"$adresse_locale[0]"}{$adresse_locale[1]}{$adresse_distante[0]}{$adresse_distante[1]} = $tab_netstat[5];
	}
}
foreach my $name (keys %correspondances) {
    foreach my $subject (keys %{ $correspondances{$name} }) {
        foreach my $tab (sort keys %{ $correspondances{$name}{$subject} }) {
        	foreach my $tablo (sort keys %{ $correspondances{$name}{$subject}{$tab} }) {
        		print "$name, $subject, $tab, $tablo  : $correspondances{$name}{$subject}{$tab}{$tablo}\n";
        	}
        }
    }
}